<?php

function foobar(Array $e)
{
    if(!empty($e)):

        $r = array(['Seu', 'Secretario'], ['Seus', 'Secretarios'], ['Sua', 'Secretaria'], ['Suas', 'Secretarias']);
        $f = false;
        $m = false;

        $count=0;
        foreach ($e as $a):
        $count++;

            if(strtolower($a) == "f"):
                $f = true;
            elseif(strtolower($a) == "m"):
                $m = true;
            endif;

        endforeach;

        if($m == true and $f == false and $count == 1):
            return $r[0];
        elseif($m == true and $f == false and $count > 1):
            return $r[1];
        elseif($m == true and $f == true and $count == 1):
            return $r[0];
        elseif($m == true and $f == true and $count > 1):
            return $r[1];
        elseif($m == false and $f == true and $count == 1):
            return $r[2];
        elseif($m == false and $f == true and $count > 1):
            return $r[3];
        endif;
    endif;
}

var_dump(foobar(['f', 'f']));